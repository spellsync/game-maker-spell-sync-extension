# SpellSync Extension for GameMaker


## Getting started

Minimum IDE version 2023.2.0.71 (Runtime 2023.2.0.87) or GameMaker LTS IDE v2022.0.1.31 (Runtime v2022.0.1.30).

Import the `spellsync-ext_vХ.Х.Х.yymps` extension into the project via ` Tools > Import Local Package `.
Set your `Project ID` and `Public Token` in the extension options.

Use the demo project `SpellSync_Demo_vX.X.X.yyz` as an example of working with the SDK.

Demo on SpellSync Hosting: [https://s3.spellsync.com/games/draft/7/v1/](https://s3.spellsync.com/games/draft/7/v1/)

The plugin inherits the hierarchy and naming convention from SDK, you can refer to the [documentation](https://docs.spellsync.com/docs/get-start/) for more information about methods work.

To perform the test, you will need a WEB server with SSL. One of the options for configuring the local server is available at [link](./manuals/local-web-server-en.md).

**ATTENTION**

When working with the plugin, the game may terminate with the error `Unhandled Rejection`. This behavior is due to the interception of unhandled Game Maker errors. You can use the [HTML5 Runtime](https://github.com/trumdu/GameMaker-HTML5-URHDisabled/releases) with the bug fixed.
