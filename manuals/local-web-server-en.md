# Setting up a local server on Windows
There are several ways to set up a local server. One of them is described below.

Install OpenSSL. You can download it from [https://slproweb.com/products/Win32OpenSSL.html](https://slproweb.com/products/Win32OpenSSL.html)
I chose the Win64 OpenSSL v1.1.1t MSI (not the light version).

![Win64 OpenSSL v1.1.1t MSI (not the light version)](./images/Win32OpenSSL.png "Win64 OpenSSL v1.1.1t")


Install npm (Node.js) by following the instructions [on Node.JS](https://nodejs.org/en). Using npm, install the http-server package:

` > npm install -g http-server `

Go to the game build catalog and create a certificate and a private key through openssl:

` > "C:\Program Files\OpenSSL-Win64\bin\openssl.exe" req -x509 -newkey rsa:4096 -sha256 -days 1024 -nodes -keyout key.pem -out cert.pem `

Run the web server:

` > http-server --ssl -c-1 -p 8080 -a 127.0.0.1 `

In the browse, open [https://localhost:8080](https://localhost:8080). When the security warning appears, click the confirmation button.