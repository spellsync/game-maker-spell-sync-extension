function get_logs() {
	var _str = "";
	for (var _i = ds_list_size(obj_log_output.log_list); _i > 0; _i -= 1)
	{
		if (_i != ds_list_size(obj_log_output.log_list)) _str += "\n";
		_str += ds_list_find_value(obj_log_output.log_list, _i - 1);
	}
	return _str;
}