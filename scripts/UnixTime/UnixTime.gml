/// @func   get_unixtime()
/// @desc   Returns Unix timestamp.
function get_unixtime(){
    var _timezone = date_get_timezone();
 
    date_set_timezone(timezone_utc);
    var _epoch = floor(date_create_datetime(1970, 1, 1, 0, 0, 0));
    
    date_set_timezone(_timezone);
    var _datetime = date_current_datetime();
    
    var _timestamp = floor(date_second_span(_epoch, _datetime));
 
    return _timestamp;
}

/// @desc  convert_unixtime(unix_timestamp)  Returns GameMaker datetime from Unix timestamp.
/// @param {real} _timestamp Description
/// @returns {real} Description
function convert_unixtime(_timestamp){
    var _timezone = date_get_timezone();
 
    date_set_timezone(timezone_utc);
    
    var _epoch = date_create_datetime(1970, 1, 1, 0, 0, _timestamp);
    var _ep_h = date_get_hour(_epoch);
    
    date_set_timezone(_timezone);
    
    var _c_h = date_get_hour(date_current_datetime());
    
    var _calc_fuse = (_c_h - _ep_h) * 3600;
    
    var _datetime = date_create_datetime(1970, 1, 1, 0, 0, _timestamp + _calc_fuse);
    return _datetime;
}