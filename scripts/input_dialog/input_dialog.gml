/// @function input_dialog(_str, _def, _callback_function);
/// @param {String} _str Data request text
/// @param {Any} _def Default value
/// @param {function}  _callback_function Callback function
function input_dialog(_str, _def, _callback_function){
	var _id = instance_create_layer(0, 0, "Dialog", obj_dialog_textbox);
	obj_dialog_textbox.header_text = _str;
	obj_dialog_textbox.input_text = string(_def);
	obj_dialog_textbox.callback_function = _callback_function;
	return _id;
}