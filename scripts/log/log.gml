/**
 * @param {any} _text Description
 * @param {bool} [_is_err]=false Description
 */
function log(_text, _is_err = false) {
	gml_pragma("forceinline");
	obj_log_output.add_log(_text, _is_err);
	//
	if (instance_exists(obj_button_LoggerWarn)) {
		if (obj_button_LoggerWarn.status > 0) {
			if (_is_err) {
				SpellSync_LoggerError(_text);	
			}
			else {
				SpellSync_LoggerInfo(_text);	
			}
		}
	}
}


/**
 * @param {string} _text Description
 * @param {bool} [_is_err]=false Description
 */
function log_event(_text, _is_err = false) {
	log("Event: " + _text, _is_err);
}