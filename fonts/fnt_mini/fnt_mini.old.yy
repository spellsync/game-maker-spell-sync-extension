{
  "resourceType": "GMFont",
  "resourceVersion": "1.0",
  "name": "fnt_mini",
  "AntiAlias": 1,
  "applyKerning": 0,
  "ascender": 9,
  "ascenderOffset": 2,
  "bold": false,
  "canGenerateBitmap": true,
  "charset": 1,
  "first": 0,
  "fontName": "Arial",
  "glyphOperations": 0,
  "glyphs": {
    "32": {"character":32,"h":14,"offset":0,"shift":3,"w":3,"x":2,"y":2,},
    "33": {"character":33,"h":14,"offset":0,"shift":3,"w":3,"x":135,"y":50,},
    "34": {"character":34,"h":14,"offset":0,"shift":4,"w":4,"x":140,"y":50,},
    "35": {"character":35,"h":14,"offset":0,"shift":6,"w":6,"x":146,"y":50,},
    "36": {"character":36,"h":14,"offset":0,"shift":6,"w":6,"x":154,"y":50,},
    "37": {"character":37,"h":14,"offset":0,"shift":10,"w":10,"x":162,"y":50,},
    "38": {"character":38,"h":14,"offset":0,"shift":7,"w":8,"x":174,"y":50,},
    "39": {"character":39,"h":14,"offset":0,"shift":2,"w":2,"x":184,"y":50,},
    "40": {"character":40,"h":14,"offset":0,"shift":4,"w":4,"x":196,"y":50,},
    "41": {"character":41,"h":14,"offset":0,"shift":4,"w":4,"x":245,"y":50,},
    "42": {"character":42,"h":14,"offset":0,"shift":4,"w":4,"x":202,"y":50,},
    "43": {"character":43,"h":14,"offset":0,"shift":6,"w":6,"x":208,"y":50,},
    "44": {"character":44,"h":14,"offset":0,"shift":3,"w":3,"x":216,"y":50,},
    "45": {"character":45,"h":14,"offset":0,"shift":4,"w":4,"x":221,"y":50,},
    "46": {"character":46,"h":14,"offset":1,"shift":3,"w":2,"x":227,"y":50,},
    "47": {"character":47,"h":14,"offset":0,"shift":3,"w":4,"x":231,"y":50,},
    "48": {"character":48,"h":14,"offset":0,"shift":6,"w":6,"x":237,"y":50,},
    "49": {"character":49,"h":14,"offset":1,"shift":6,"w":4,"x":129,"y":50,},
    "50": {"character":50,"h":14,"offset":0,"shift":6,"w":6,"x":188,"y":50,},
    "51": {"character":51,"h":14,"offset":0,"shift":6,"w":6,"x":121,"y":50,},
    "52": {"character":52,"h":14,"offset":0,"shift":6,"w":6,"x":19,"y":50,},
    "53": {"character":53,"h":14,"offset":0,"shift":6,"w":6,"x":221,"y":34,},
    "54": {"character":54,"h":14,"offset":0,"shift":6,"w":6,"x":229,"y":34,},
    "55": {"character":55,"h":14,"offset":0,"shift":6,"w":6,"x":237,"y":34,},
    "56": {"character":56,"h":14,"offset":0,"shift":6,"w":6,"x":245,"y":34,},
    "57": {"character":57,"h":14,"offset":0,"shift":6,"w":6,"x":2,"y":50,},
    "58": {"character":58,"h":14,"offset":1,"shift":3,"w":2,"x":10,"y":50,},
    "59": {"character":59,"h":14,"offset":0,"shift":3,"w":3,"x":14,"y":50,},
    "60": {"character":60,"h":14,"offset":0,"shift":6,"w":6,"x":27,"y":50,},
    "61": {"character":61,"h":14,"offset":0,"shift":6,"w":6,"x":104,"y":50,},
    "62": {"character":62,"h":14,"offset":0,"shift":6,"w":6,"x":35,"y":50,},
    "63": {"character":63,"h":14,"offset":0,"shift":6,"w":6,"x":43,"y":50,},
    "64": {"character":64,"h":14,"offset":0,"shift":11,"w":11,"x":51,"y":50,},
    "65": {"character":65,"h":14,"offset":-1,"shift":7,"w":9,"x":64,"y":50,},
    "66": {"character":66,"h":14,"offset":0,"shift":7,"w":7,"x":75,"y":50,},
    "67": {"character":67,"h":14,"offset":0,"shift":8,"w":8,"x":84,"y":50,},
    "68": {"character":68,"h":14,"offset":0,"shift":8,"w":8,"x":94,"y":50,},
    "69": {"character":69,"h":14,"offset":0,"shift":7,"w":7,"x":112,"y":50,},
    "70": {"character":70,"h":14,"offset":0,"shift":7,"w":7,"x":10,"y":66,},
    "71": {"character":71,"h":14,"offset":0,"shift":9,"w":8,"x":145,"y":66,},
    "72": {"character":72,"h":14,"offset":0,"shift":8,"w":8,"x":19,"y":66,},
    "73": {"character":73,"h":14,"offset":1,"shift":3,"w":2,"x":170,"y":66,},
    "74": {"character":74,"h":14,"offset":0,"shift":6,"w":5,"x":174,"y":66,},
    "75": {"character":75,"h":14,"offset":0,"shift":7,"w":8,"x":181,"y":66,},
    "76": {"character":76,"h":14,"offset":0,"shift":6,"w":6,"x":191,"y":66,},
    "77": {"character":77,"h":14,"offset":0,"shift":9,"w":9,"x":199,"y":66,},
    "78": {"character":78,"h":14,"offset":0,"shift":8,"w":8,"x":210,"y":66,},
    "79": {"character":79,"h":14,"offset":0,"shift":9,"w":9,"x":220,"y":66,},
    "80": {"character":80,"h":14,"offset":0,"shift":7,"w":7,"x":240,"y":66,},
    "81": {"character":81,"h":14,"offset":0,"shift":9,"w":9,"x":73,"y":82,},
    "82": {"character":82,"h":14,"offset":0,"shift":8,"w":8,"x":2,"y":82,},
    "83": {"character":83,"h":14,"offset":0,"shift":7,"w":7,"x":12,"y":82,},
    "84": {"character":84,"h":14,"offset":0,"shift":7,"w":7,"x":21,"y":82,},
    "85": {"character":85,"h":14,"offset":0,"shift":8,"w":8,"x":30,"y":82,},
    "86": {"character":86,"h":14,"offset":0,"shift":7,"w":8,"x":40,"y":82,},
    "87": {"character":87,"h":14,"offset":0,"shift":10,"w":11,"x":50,"y":82,},
    "88": {"character":88,"h":14,"offset":0,"shift":7,"w":8,"x":63,"y":82,},
    "89": {"character":89,"h":14,"offset":0,"shift":7,"w":8,"x":160,"y":66,},
    "90": {"character":90,"h":14,"offset":0,"shift":7,"w":7,"x":231,"y":66,},
    "91": {"character":91,"h":14,"offset":0,"shift":3,"w":3,"x":155,"y":66,},
    "92": {"character":92,"h":14,"offset":0,"shift":3,"w":4,"x":80,"y":66,},
    "93": {"character":93,"h":14,"offset":0,"shift":3,"w":3,"x":29,"y":66,},
    "94": {"character":94,"h":14,"offset":0,"shift":5,"w":5,"x":34,"y":66,},
    "95": {"character":95,"h":14,"offset":-1,"shift":6,"w":8,"x":41,"y":66,},
    "96": {"character":96,"h":14,"offset":0,"shift":4,"w":3,"x":51,"y":66,},
    "97": {"character":97,"h":14,"offset":0,"shift":6,"w":6,"x":56,"y":66,},
    "98": {"character":98,"h":14,"offset":0,"shift":6,"w":6,"x":64,"y":66,},
    "99": {"character":99,"h":14,"offset":0,"shift":6,"w":6,"x":72,"y":66,},
    "100": {"character":100,"h":14,"offset":0,"shift":6,"w":6,"x":86,"y":66,},
    "101": {"character":101,"h":14,"offset":0,"shift":6,"w":6,"x":137,"y":66,},
    "102": {"character":102,"h":14,"offset":0,"shift":3,"w":4,"x":94,"y":66,},
    "103": {"character":103,"h":14,"offset":0,"shift":6,"w":6,"x":100,"y":66,},
    "104": {"character":104,"h":14,"offset":0,"shift":6,"w":6,"x":108,"y":66,},
    "105": {"character":105,"h":14,"offset":0,"shift":2,"w":2,"x":116,"y":66,},
    "106": {"character":106,"h":14,"offset":-1,"shift":2,"w":3,"x":120,"y":66,},
    "107": {"character":107,"h":14,"offset":0,"shift":6,"w":6,"x":125,"y":66,},
    "108": {"character":108,"h":14,"offset":0,"shift":2,"w":2,"x":133,"y":66,},
    "109": {"character":109,"h":14,"offset":0,"shift":9,"w":9,"x":210,"y":34,},
    "110": {"character":110,"h":14,"offset":0,"shift":6,"w":6,"x":2,"y":66,},
    "111": {"character":111,"h":14,"offset":0,"shift":6,"w":6,"x":202,"y":34,},
    "112": {"character":112,"h":14,"offset":0,"shift":6,"w":6,"x":87,"y":18,},
    "113": {"character":113,"h":14,"offset":0,"shift":6,"w":6,"x":204,"y":2,},
    "114": {"character":114,"h":14,"offset":0,"shift":4,"w":4,"x":212,"y":2,},
    "115": {"character":115,"h":14,"offset":0,"shift":6,"w":6,"x":218,"y":2,},
    "116": {"character":116,"h":14,"offset":0,"shift":3,"w":3,"x":226,"y":2,},
    "117": {"character":117,"h":14,"offset":0,"shift":6,"w":6,"x":231,"y":2,},
    "118": {"character":118,"h":14,"offset":0,"shift":6,"w":6,"x":239,"y":2,},
    "119": {"character":119,"h":14,"offset":0,"shift":8,"w":8,"x":2,"y":18,},
    "120": {"character":120,"h":14,"offset":0,"shift":6,"w":6,"x":20,"y":18,},
    "121": {"character":121,"h":14,"offset":0,"shift":6,"w":6,"x":79,"y":18,},
    "122": {"character":122,"h":14,"offset":0,"shift":6,"w":6,"x":28,"y":18,},
    "123": {"character":123,"h":14,"offset":0,"shift":4,"w":4,"x":36,"y":18,},
    "124": {"character":124,"h":14,"offset":1,"shift":3,"w":1,"x":42,"y":18,},
    "125": {"character":125,"h":14,"offset":0,"shift":4,"w":4,"x":45,"y":18,},
    "126": {"character":126,"h":14,"offset":0,"shift":6,"w":6,"x":51,"y":18,},
    "1040": {"character":1040,"h":14,"offset":-1,"shift":7,"w":9,"x":59,"y":18,},
    "1041": {"character":1041,"h":14,"offset":0,"shift":7,"w":7,"x":70,"y":18,},
    "1042": {"character":1042,"h":14,"offset":0,"shift":7,"w":7,"x":195,"y":2,},
    "1043": {"character":1043,"h":14,"offset":0,"shift":6,"w":6,"x":12,"y":18,},
    "1044": {"character":1044,"h":14,"offset":0,"shift":7,"w":8,"x":185,"y":2,},
    "1045": {"character":1045,"h":14,"offset":0,"shift":7,"w":7,"x":78,"y":2,},
    "1046": {"character":1046,"h":14,"offset":0,"shift":10,"w":11,"x":7,"y":2,},
    "1047": {"character":1047,"h":14,"offset":0,"shift":7,"w":7,"x":20,"y":2,},
    "1048": {"character":1048,"h":14,"offset":0,"shift":8,"w":8,"x":29,"y":2,},
    "1049": {"character":1049,"h":14,"offset":0,"shift":8,"w":8,"x":39,"y":2,},
    "1050": {"character":1050,"h":14,"offset":0,"shift":6,"w":7,"x":49,"y":2,},
    "1051": {"character":1051,"h":14,"offset":0,"shift":7,"w":7,"x":58,"y":2,},
    "1052": {"character":1052,"h":14,"offset":0,"shift":9,"w":9,"x":67,"y":2,},
    "1053": {"character":1053,"h":14,"offset":0,"shift":8,"w":8,"x":87,"y":2,},
    "1054": {"character":1054,"h":14,"offset":0,"shift":9,"w":9,"x":164,"y":2,},
    "1055": {"character":1055,"h":14,"offset":0,"shift":8,"w":8,"x":97,"y":2,},
    "1056": {"character":1056,"h":14,"offset":0,"shift":7,"w":7,"x":107,"y":2,},
    "1057": {"character":1057,"h":14,"offset":0,"shift":8,"w":8,"x":116,"y":2,},
    "1058": {"character":1058,"h":14,"offset":0,"shift":7,"w":7,"x":126,"y":2,},
    "1059": {"character":1059,"h":14,"offset":0,"shift":7,"w":7,"x":135,"y":2,},
    "1060": {"character":1060,"h":14,"offset":0,"shift":8,"w":8,"x":144,"y":2,},
    "1061": {"character":1061,"h":14,"offset":0,"shift":7,"w":8,"x":154,"y":2,},
    "1062": {"character":1062,"h":14,"offset":0,"shift":8,"w":8,"x":175,"y":2,},
    "1063": {"character":1063,"h":14,"offset":0,"shift":7,"w":7,"x":95,"y":18,},
    "1064": {"character":1064,"h":14,"offset":0,"shift":10,"w":10,"x":10,"y":34,},
    "1065": {"character":1065,"h":14,"offset":0,"shift":10,"w":10,"x":104,"y":18,},
    "1066": {"character":1066,"h":14,"offset":0,"shift":9,"w":9,"x":38,"y":34,},
    "1067": {"character":1067,"h":14,"offset":0,"shift":10,"w":9,"x":49,"y":34,},
    "1068": {"character":1068,"h":14,"offset":0,"shift":7,"w":7,"x":60,"y":34,},
    "1069": {"character":1069,"h":14,"offset":0,"shift":8,"w":8,"x":69,"y":34,},
    "1070": {"character":1070,"h":14,"offset":0,"shift":11,"w":11,"x":79,"y":34,},
    "1071": {"character":1071,"h":14,"offset":0,"shift":8,"w":8,"x":92,"y":34,},
    "1072": {"character":1072,"h":14,"offset":0,"shift":6,"w":6,"x":102,"y":34,},
    "1073": {"character":1073,"h":14,"offset":0,"shift":6,"w":6,"x":118,"y":34,},
    "1074": {"character":1074,"h":14,"offset":0,"shift":6,"w":6,"x":184,"y":34,},
    "1075": {"character":1075,"h":14,"offset":0,"shift":4,"w":5,"x":126,"y":34,},
    "1076": {"character":1076,"h":14,"offset":0,"shift":6,"w":7,"x":133,"y":34,},
    "1077": {"character":1077,"h":14,"offset":0,"shift":6,"w":6,"x":142,"y":34,},
    "1078": {"character":1078,"h":14,"offset":-1,"shift":7,"w":9,"x":150,"y":34,},
    "1079": {"character":1079,"h":14,"offset":0,"shift":5,"w":5,"x":161,"y":34,},
    "1080": {"character":1080,"h":14,"offset":0,"shift":6,"w":6,"x":168,"y":34,},
    "1081": {"character":1081,"h":14,"offset":0,"shift":6,"w":6,"x":176,"y":34,},
    "1082": {"character":1082,"h":14,"offset":0,"shift":5,"w":5,"x":31,"y":34,},
    "1083": {"character":1083,"h":14,"offset":0,"shift":6,"w":6,"x":110,"y":34,},
    "1084": {"character":1084,"h":14,"offset":0,"shift":8,"w":7,"x":22,"y":34,},
    "1085": {"character":1085,"h":14,"offset":0,"shift":6,"w":6,"x":174,"y":18,},
    "1086": {"character":1086,"h":14,"offset":0,"shift":6,"w":6,"x":116,"y":18,},
    "1087": {"character":1087,"h":14,"offset":0,"shift":6,"w":6,"x":124,"y":18,},
    "1088": {"character":1088,"h":14,"offset":0,"shift":6,"w":6,"x":132,"y":18,},
    "1089": {"character":1089,"h":14,"offset":0,"shift":6,"w":6,"x":140,"y":18,},
    "1090": {"character":1090,"h":14,"offset":0,"shift":5,"w":5,"x":148,"y":18,},
    "1091": {"character":1091,"h":14,"offset":0,"shift":6,"w":6,"x":155,"y":18,},
    "1092": {"character":1092,"h":14,"offset":0,"shift":9,"w":9,"x":163,"y":18,},
    "1093": {"character":1093,"h":14,"offset":0,"shift":6,"w":6,"x":182,"y":18,},
    "1094": {"character":1094,"h":14,"offset":0,"shift":6,"w":6,"x":2,"y":34,},
    "1095": {"character":1095,"h":14,"offset":0,"shift":6,"w":5,"x":190,"y":18,},
    "1096": {"character":1096,"h":14,"offset":0,"shift":9,"w":9,"x":197,"y":18,},
    "1097": {"character":1097,"h":14,"offset":0,"shift":9,"w":9,"x":208,"y":18,},
    "1098": {"character":1098,"h":14,"offset":0,"shift":7,"w":7,"x":219,"y":18,},
    "1099": {"character":1099,"h":14,"offset":0,"shift":8,"w":8,"x":228,"y":18,},
    "1100": {"character":1100,"h":14,"offset":0,"shift":6,"w":6,"x":238,"y":18,},
    "1101": {"character":1101,"h":14,"offset":0,"shift":6,"w":6,"x":246,"y":18,},
    "1102": {"character":1102,"h":14,"offset":0,"shift":8,"w":8,"x":192,"y":34,},
    "1103": {"character":1103,"h":14,"offset":0,"shift":6,"w":6,"x":84,"y":82,},
  },
  "hinting": 0,
  "includeTTF": false,
  "interpreter": 0,
  "italic": false,
  "kerningPairs": [
    {"amount":-1,"first":70,"second":44,},
    {"amount":-1,"first":70,"second":46,},
    {"amount":-1,"first":80,"second":44,},
    {"amount":-1,"first":80,"second":46,},
    {"amount":-1,"first":84,"second":44,},
    {"amount":-1,"first":84,"second":46,},
    {"amount":-1,"first":84,"second":58,},
    {"amount":-1,"first":84,"second":59,},
    {"amount":-1,"first":84,"second":97,},
    {"amount":-1,"first":84,"second":99,},
    {"amount":-1,"first":84,"second":101,},
    {"amount":-1,"first":84,"second":111,},
    {"amount":-1,"first":84,"second":115,},
    {"amount":-1,"first":84,"second":894,},
    {"amount":-1,"first":89,"second":44,},
    {"amount":-1,"first":89,"second":46,},
    {"amount":-1,"first":1043,"second":44,},
    {"amount":-1,"first":1043,"second":46,},
    {"amount":-1,"first":1056,"second":44,},
    {"amount":-1,"first":1056,"second":46,},
    {"amount":-1,"first":1058,"second":44,},
    {"amount":-1,"first":1058,"second":46,},
    {"amount":-1,"first":1059,"second":44,},
    {"amount":-1,"first":1059,"second":46,},
    {"amount":-1,"first":1075,"second":44,},
    {"amount":-1,"first":1075,"second":46,},
    {"amount":-1,"first":1090,"second":44,},
    {"amount":-1,"first":1090,"second":46,},
  ],
  "last": 0,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "pointRounding": 0,
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":1040,"upper":1103,},
  ],
  "regenerateBitmap": false,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\n\nDefault Character(9647) ▯",
  "size": 8.0,
  "styleName": "Regular",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "TTFName": "",
}