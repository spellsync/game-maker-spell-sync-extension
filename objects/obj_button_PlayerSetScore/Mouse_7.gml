if (is_clicked) {
	input_dialog("Player Set Score", 100, function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Player set Score: " + string(_result);
			log(_msg);
			SpellSync_PlayerSetScore(_result);
		}
	});
};