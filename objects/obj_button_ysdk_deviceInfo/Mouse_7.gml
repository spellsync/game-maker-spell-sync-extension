if (is_clicked) {
	if (SpellSync_PlatformType() != SpellSync_PlatformTypeYANDEX) {
		log("The method is only for the Yandex Games platform.", true);
	}
	else {
		log("Call native yandex deviceInfo.");
		var msg = "Device Info: " + SpellSync_PlatformGetNativeSDK("deviceInfo");	
	    log(msg);
	}
};