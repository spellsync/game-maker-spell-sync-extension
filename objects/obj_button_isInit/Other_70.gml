/// @description Player Readiness event
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Waiting for the player data readiness event
		if (async_load[? "event"] == SpellSync_CallPlayerReady) {
			check_and_activate_buttons();
		}
   }
}
