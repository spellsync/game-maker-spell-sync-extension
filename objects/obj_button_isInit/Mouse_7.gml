if (is_clicked) {
	var _st = SpellSync_InitStatus();
	var _msg = "SpellSync SDK isInit: " + ((_st > 0) ? "true" : "false");
    log(_msg);
	if (_st == 1) {
		with (obj_button_parent) {
			is_disabled = false;	
		}
	}
	else {
		_msg = "Attention! The SDK is downloaded only via HTTPS connection. Use a third-party web server for debugging.";
		log(_msg, true);
	}
};