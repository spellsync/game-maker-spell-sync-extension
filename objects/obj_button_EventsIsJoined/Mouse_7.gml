if (is_clicked) {
	input_dialog("Event Id or Tag", "event_test", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Events Is Joined " + _result + ": ";
			_msg += SpellSync_EventsIsJoined(_result) > 0 ? "Yes" : "No";
			log(_msg);
		}
	});
};