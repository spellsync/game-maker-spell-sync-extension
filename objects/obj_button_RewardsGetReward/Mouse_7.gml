if (is_clicked) {
	input_dialog("Reward Id or Tag", "test_reward", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _json = SpellSync_RewardsGetReward(_result);
			var _msg = "Get Reward " + _result + ": ";
			if (string_length(_json) > 0) {
				_msg += _json;
				// var r = json_parse(_json);
				// r.rewards.id
				// r.rewards.tag
				// r.reward.name
				// r.reward.description
				// r.reward.isAutoAccept
				// r.reward.mutations
				//
				// r.playerReward.rewardId
				// r.playerReward.countTotal
				// r.playerReward.countAccepted
			}
			else {
				_msg += "reward_not_found";
			}
			log(_msg);
		}
	});
};