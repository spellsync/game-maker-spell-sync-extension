if (is_clicked) {
	input_dialog("Payments Consume", "test", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Payments Consume: " + string(_result);
			log(_msg);
			SpellSync_PaymentsConsume(_result);
		}
	});
};