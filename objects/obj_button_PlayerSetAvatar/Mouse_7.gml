if (is_clicked) {
	input_dialog("Player Avatar URL", "https://server.org/new_avatar.png", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Player set Avatar: " + string(_result);
			log(_msg);
			SpellSync_PlayerSetAvatar(_result);
		}
	});
};