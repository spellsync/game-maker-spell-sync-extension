if (is_clicked) {
	var _msg = "";
	if (SpellSync_IsDev() == 0) {
		_msg = "Logger functions are only available for links in development mode.";
	}
	else {
		status = !status;
		_msg = "Logger \"" + st_text[status] + "\"";
	}
	log(_msg);
	st_func();
};