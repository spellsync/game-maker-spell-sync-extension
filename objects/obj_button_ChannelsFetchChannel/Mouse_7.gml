if (is_clicked) {
    log("Channels Fetch Channel");
	input_dialog("Channel ID", channelID, function() {
		if (string_length(input_result) > 0) {
			//
			channelID = string_digits(input_result);
			SpellSync_ChannelsFetchChannel(channelID);
		};
	});
};