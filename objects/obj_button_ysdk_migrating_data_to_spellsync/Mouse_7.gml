if (is_clicked) {
	if (SpellSync_PlatformType() != SpellSync_PlatformTypeYANDEX) {
		log("The method is only for the Yandex Games platform.", true);
	}
	else {
		log("Initialization of player data in Yandex");
		SpellSync_PlatformGetNativeSDK("player=getPlayer", req_event_player);	
	}
};