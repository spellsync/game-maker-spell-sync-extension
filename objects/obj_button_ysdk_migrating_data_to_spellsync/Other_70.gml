/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		//
		switch (async_load[? "event"])
		{
			case req_event_player:
				// The player is initialized. Loading Player Data
				log("The player is initialized. Loading Player Data");
				SpellSync_PlatformGetNativeSDK("player:getData", req_event_data);	
			break;
			case req_event_data:
				// Player data is loaded
				log("Yandex Player data is loaded");
				var _json = async_load[? "value"];
				try {
					var oldSave = json_parse(_json);
					if (oldSave.level > 0) {
						SpellSync_PlayerSet("level", string(oldSave.level));
						SpellSync_PlayerSet("progress", string(oldSave.progress));
						log("Migration completed");
					}
					else {
						log("Migration is not required");
					}
				}
				catch(error){
					log(error);
				}			
			break;
			case SpellSync_CallPlatformGetNativeSDKError:
				//
			break;
		}
   }
}

