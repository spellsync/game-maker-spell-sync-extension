if (is_clicked) {
	var _msg = get_logs();
	var _st = SpellSync_Tools_copyTextToClipboard(_msg);
	if (_st > 0) {
		log("The log has been copied to the clipboard.");
	}
	else {
		/*
		This function is only valid on the Windows, Android, MacOS, iOS, HTML5 and Opera GX targets.
		On HTML5, clipboard are not supported on Firefox without the use of an extension, 
		and are not supported on Internet Explorer at all.
		*/
		clipboard_set_text(_msg);
	}
};