/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// By default, Game Maker intercepts all input in the window.
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnOpenChat:
				// Disabling text input interception.
				browser_input_capture(false);
			break;
			case SpellSync_CallOnCloseChat:
				// Enabling text input interception.
				browser_input_capture(true);
			break;
			case SpellSync_CallOnOpenChatError:
				var _error = async_load[? "value"];
				//log(_error);
			break;
		}
   }
}

