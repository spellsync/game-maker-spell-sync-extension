/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallAppRequestReview:
				var _rating = async_load[? "value"];
			break;
			case SpellSync_CallAppRequestReviewError:
				var _error = async_load[? "value"];
			break;
		}
   }
}

