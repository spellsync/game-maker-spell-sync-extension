if (is_clicked) {
	if (SpellSync_PlatformType() != SpellSync_PlatformTypeYANDEX) {
		log("The method is only for the Yandex Games platform.", true);
	}
	else {
		log("Call native yandex setLeaderboardScore.");
		var lbName = "score";
		var lbEntry = json_stringify({
			includeUser: true,
			quantityAround: 3,
			quantityTop: 10
		});
		SpellSync_PlatformGetNativeSDK("lb:getLeaderboardEntries", req_event, lbName, lbEntry);
	}
};