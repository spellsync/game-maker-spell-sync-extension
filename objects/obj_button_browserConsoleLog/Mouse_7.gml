if (is_clicked) {
	var _st = get_logs();
	var _debug = SpellSync_Tools_getDebugMode();
	if (!_debug) SpellSync_Tools_setDebugMode(1);
	SpellSync_Tools_browserConsoleLog(_st);
	if (!_debug) SpellSync_Tools_setDebugMode(0);
};