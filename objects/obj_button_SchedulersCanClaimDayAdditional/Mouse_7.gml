if (is_clicked) {
	input_dialog("Scheduler Id or Tag", "test_schedulers", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Schedulers Can Claim Day #3 Additional " + _result + ": ";
			_msg += SpellSync_SchedulersCanClaimDayAdditional(_result, 3, "test_additional") > 0 ? "Yes" : "No";
			log(_msg);
		}
	});
};