if (is_clicked) {
	if (SpellSync_PlatformType() != SpellSync_PlatformTypeYANDEX) {
		log("The method is only for the Yandex Games platform.", true);
	}
	else {
		log("Call native yandex setLeaderboardScore.");
		var lbName = "score";
		var lbScore = string(200);
		SpellSync_PlatformGetNativeSDK("lb:setLeaderboardScore", req_event, lbName, lbScore);	
	}
};