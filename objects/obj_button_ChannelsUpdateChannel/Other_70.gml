/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnUpdateChannel:
				var _json = async_load[? "value"];
				var channel = json_parse(_json);
				/*
			    channel.id;
			    channel.tags;
			    channel.messageTags;
			    channel.capacity;
			    channel.ownerId;
			    channel.name;
			    channel.description;
			    channel.private;
			    channel.visible;
			    channel.hasPassword;
			    channel.ownerAcl;
			    channel.memberAcl;
			    channel.guestAcl;
				*/
				//log(channel);
			break;
			case SpellSync_CallOnUpdateChannelError:
				var _error = async_load[? "value"];
				//log(_error);
			break;
		}
   }
}

