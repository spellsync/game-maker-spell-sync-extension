if (is_clicked) {
    log("Create Channel");
	input_dialog("Channel ID", 0, function() {
		if (string_length(input_result) > 0) {
		    var _result = string_digits(input_result);
			var _chanel = {
				channelId: _result,
				tags: [
					"dungeon", 
					"x5", 
					"demons_lord_castle", 
					"heroic", 
				],
				capacity: 33,
				name: "Подземелье Demons Lord Castle",
				description: "Фаст пронос, нужны все",
				private: true,
				visible: true,
				password: "",
				ownerAcl: {
				    canViewMessages: true,
				    canAddMessage: true,
				    canEditMessage: true,
				    canDeleteMessage: true,
				    canViewMembers: true,
				    canInvitePlayer: true,
				    canKickPlayer: true,
				    canAcceptJoinRequest: true,
				    canMutePlayer: true,
				},	
				memberAcl: {
				    canViewMessages: true,
				    canAddMessage: true,
				    canEditMessage: true,
				    canDeleteMessage: true,
				    canViewMembers: true,
				    canInvitePlayer: true,
				    canKickPlayer: true,
				    canAcceptJoinRequest: true,
				    canMutePlayer: true,
				},	
				guestAcl: {
				    canViewMessages: true,
				    canAddMessage: true,
				    canEditMessage: true,
				    canDeleteMessage: true,
				    canViewMembers: true,
				    canInvitePlayer: true,
				    canKickPlayer: true,
				    canAcceptJoinRequest: true,
				    canMutePlayer: true,
				},		
			};
			var _json_data = json_stringify(_chanel);
			SpellSync_ChannelsUpdateChannel(_json_data);
		}
	});
};