input_dialog("Player key variant value", "one", function() {
	if (string_length(input_result) > 0) {
	    var _result = is_numeric(input_result) ? real(input_result) : input_result;
		var _msg = "Player key variant value: " + string(_result);
		log(_msg);
		_msg = "Player field variant name: " + SpellSync_PlayerGetFieldVariantName(key, _result);
		log(_msg);
	}
});