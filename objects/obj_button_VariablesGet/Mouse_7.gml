if (is_clicked) {
	input_dialog("Variables key", key, function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Variables key: " + _result;
			log(_msg);
			_msg = string(SpellSync_VariablesGet(_result));
			_msg = "Variables " + string(_result) + ": " + _msg;
		    log(_msg);
		}
	});
};