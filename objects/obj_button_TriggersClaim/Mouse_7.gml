if (is_clicked) {
	input_dialog("Trigger Tag", "test_trigger", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Triggers Claim: " + _result;
			log(_msg);
			SpellSync_TriggersClaim("", _result);
		}
	});
};