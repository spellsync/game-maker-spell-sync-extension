if (is_clicked) {
	input_dialog("Payments Subscribe", "test_s", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Payments Subscribe: " + string(_result);
			log(_msg);
			SpellSync_PaymentsSubscribe(_result);
		}
	});
};