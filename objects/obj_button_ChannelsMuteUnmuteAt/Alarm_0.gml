input_dialog("Player ID", 0, function() {
	if (string_length(input_result) > 0) {
		//
		var playerID = string_digits(input_result);
		var _gm_datetime = date_inc_second(date_current_datetime(), 30);
		var _iso_datetime = convert_datetime_to_iso8601(_gm_datetime);
		SpellSync_ChannelsMuteUnmuteAt(channelID, playerID, _iso_datetime);
	};
});