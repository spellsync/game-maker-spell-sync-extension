if (is_clicked) {
	input_dialog("File URL", "https://s3.spellsync.com/games/files/4/ugc/6/const.txt", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "File URL: " + string(_result);
			log(_msg);
		    log("Files Load Content");
			SpellSync_FilesLoadContent(_result);
		}
	});
};