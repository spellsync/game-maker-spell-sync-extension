if (is_clicked) {
	input_dialog("Files Type", ".txt, .png", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Files Type: " + string(_result);
			log(_msg);
		    log("Files Choose File");
			SpellSync_FilesChooseFile(_result);
		}
	});
};