/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnFetchMoreSentJoinRequestsCanLoadMore:
				var _canLoadMore = async_load[? "value"];
			break;
			case SpellSync_CallOnFetchMoreSentJoinRequests:
				var _json = async_load[? "value"];
				var items = json_parse(_json);
				/*
				for (var _i = 0; _i < array_length(items); _i++) {
					var joinRequest = items[_i];
				    joinRequest.channel
				    joinRequest.channel.id
				    joinRequest.channel.tags
				    joinRequest.channel.projectId
				    joinRequest.channel.capacity
				    joinRequest.channel.ownerId
				    joinRequest.channel.name
				    joinRequest.channel.description
				    joinRequest.channel.private
				    joinRequest.channel.visible
				    joinRequest.channel.hasPassword
				    joinRequest.channel.membersCount
				    joinRequest.date
				}
				*/
			break;
			case SpellSync_CallOnFetchMoreSentJoinRequestsError:
				var _error = async_load[? "value"];
				//log(_error);
			break;
		}
   }
}

