input_dialog("Analytics Goal value", "15", function() {
	if (string_length(input_result) > 0) {
	    var _result = input_result;
		var _msg = "Analytics Goal value: " + string(_result);
		log(_msg);
		SpellSync_AnalyticsGoal(key, _result);
	}
});