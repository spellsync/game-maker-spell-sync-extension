if (is_clicked) {
	input_dialog("File URL", "data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", function() {
		if (string_length(input_result) > 0) {
			var _url = input_result;
			var _msg = "Files URL: " + string(_url);
			log(_msg);
	        var _tags = "replay,level7";
			var _msg = "Files Tags: " + string(_tags);
			log(_msg);
		    log("Files Upload URL");
			SpellSync_FilesUploadUrl(_url, "test.png", _tags);
		}
	});
};