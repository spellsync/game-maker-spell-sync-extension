if (is_clicked) {
	input_dialog("Analytics Hit url", "/my-page/example?query=1", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Analytics Hit url: " + string(_result);
			log(_msg);
			SpellSync_AnalyticsHit(_result);
		}
	});
};