if (is_clicked) {
    log("Channels Leave");
	input_dialog("Channel ID", channelID, function() {
		if (string_length(input_result) > 0) {
			//
			channelID = string_digits(input_result);
			SpellSync_ChannelsLeave(channelID);
		};
	});
};