var _a = draw_get_alpha();
var _c = draw_get_color();
//
draw_set_alpha(0.2);
draw_set_color(c_black);
draw_rectangle(0, 0, room_width, room_height, 0);
draw_set_alpha(1);
draw_set_color(c_black);
draw_roundrect(x - sprite_width_half - 7, y - sprite_height_half - 19, x + sprite_width_half + 7, y + sprite_height_half + sprite_get_height(spr_button) + 7, 0);
draw_set_color(c_white);
draw_roundrect(x - sprite_width_half - 5, y - sprite_height_half - 17, x + sprite_width_half + 5, y + sprite_height_half + sprite_get_height(spr_button) + 5, 0);
//
draw_sprite(sprite_index, -1, x, y + 9);
// Header text
draw_set_halign(fa_center);
draw_set_valign(fa_bottom);
draw_set_font(fnt_mini);
draw_set_color(c_black);
var _w = round(sprite_width * 1) / string_width(header_text);
if (_w > 1) _w = 1;
var _h = round(sprite_height * 1.5) / string_height(header_text);
if (_h > 1) _h = 1;
draw_text_transformed(x, y - sprite_height_half + 4, header_text, _w, _h, 1);
// Input text
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
var _t = input_text;
if (string_width(_t) >= (sprite_get_width(sprite_index) * 1.35)) {
	_t = string_insert("\n", _t, round(string_length(_t) / 2));	
}
_w = round(sprite_width) / string_width(_t);
if (_w > 1) _w = 1;
_h = round(sprite_height) / string_height(_t);
if (_h > 1) _h = 1;
draw_text_transformed(x, y + 9, _t, _w, _h, 1);
//
draw_set_alpha(_a);
draw_set_color(_c);