if (!is_disabled) {
	if (string_length(keyboard_string) < input_limit) {
	    input_text = keyboard_string;
	}
	else {
	    keyboard_string = input_text;
	}
	//
	if (keyboard_check(vk_control) and keyboard_check_released(ord("V"))) {
	    keyboard_string = clipboard_get_text();
		if (string_length(keyboard_string) > input_limit) {
		    keyboard_string = string_copy(keyboard_string, 1, input_limit);
		}
	    input_text = keyboard_string;
	}
	if (keyboard_check(vk_delete)) {
	    keyboard_string = "";
	    input_text = "";
	}
}