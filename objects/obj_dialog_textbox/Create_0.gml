x = 416;
y = 288;
header_text = "Enter the text";
input_text = "";
input_limit = 1024;
callback_function = function() {
	log("The function is not defined.");	
};
//
if (!variable_global_exists("input_result")) {
	globalvar input_result;
}
input_result = "";	
alarm[0] = 1;
is_disabled = true;
//
sprite_width_half = round(sprite_width / 2);
sprite_height_half = round(sprite_height / 2);
button_ok = instance_create_depth(x - 106, y + 50, depth - 1, obj_dialog_ok);
button_cancel = instance_create_depth(x - 106 + 20 + sprite_get_width(spr_button_short), y + 50, depth - 1, obj_dialog_cancel);
//
input_ok = function() {
	input_result = input_text;
	callback_function();
	instance_destroy();
}
input_cancel = function() {
	input_result = "";
	callback_function();
	instance_destroy();
}