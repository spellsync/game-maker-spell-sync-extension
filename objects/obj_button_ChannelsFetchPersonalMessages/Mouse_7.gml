if (is_clicked) {
    log("Channels Fetch Personal Messages");
	input_dialog("Personal ID", playerID, function() {
		if (string_length(input_result) > 0) {
			//
			playerID = string_digits(input_result);
			SpellSync_ChannelsFetchPersonalMessages(playerID);
		};
	});
};