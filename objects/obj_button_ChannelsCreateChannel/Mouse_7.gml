if (is_clicked) {
    log("Create Channel");

	var _chanel = {
		template: "DUNGEON_5",
		tags: [
			"dungeon", 
			"x5", 
			"demons_lord_castle", 
			"heroic", 
		],
		capacity: 5,
		name: "Подземелье Demons Lord Castle",
		description: "Фаст пронос, нужны все",
		private: true,
		visible: true,
		password: "",
		ownerAcl: {
		    canViewMessages: true,
		    canAddMessage: true,
		    canEditMessage: true,
		    canDeleteMessage: true,
		    canViewMembers: true,
		    canInvitePlayer: true,
		    canKickPlayer: true,
		    canAcceptJoinRequest: true,
		    canMutePlayer: true,
		},	
		memberAcl: {
		    canViewMessages: true,
		    canAddMessage: true,
		    canEditMessage: true,
		    canDeleteMessage: true,
		    canViewMembers: true,
		    canInvitePlayer: true,
		    canKickPlayer: true,
		    canAcceptJoinRequest: true,
		    canMutePlayer: true,
		},	
		guestAcl: {
		    canViewMessages: true,
		    canAddMessage: true,
		    canEditMessage: true,
		    canDeleteMessage: true,
		    canViewMembers: true,
		    canInvitePlayer: true,
		    canKickPlayer: true,
		    canAcceptJoinRequest: true,
		    canMutePlayer: true,
		},		
	};
	var _json_data = json_stringify(_chanel);
	SpellSync_ChannelsCreateChannel(_json_data);
};