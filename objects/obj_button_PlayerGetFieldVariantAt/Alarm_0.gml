input_dialog("Player key variant value", 1, function() {
	if (string_length(input_result) > 0) {
	    var _result = real(input_result);
		var _msg = "Player key variant value: " + string(_result);
		log(_msg);
		_msg = "Player field variant key: " + SpellSync_PlayerGetFieldVariantAt(key, _result);
		log(_msg);
	}
});