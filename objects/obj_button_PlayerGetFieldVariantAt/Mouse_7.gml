if (is_clicked) {
	input_dialog("Player field key", key, function() {
		if (string_length(input_result) > 0) {
	        key = input_result;
			var _msg = "Player field key: " + string(key);
			log(_msg);
			alarm[0] = 1;
		}
	});
};