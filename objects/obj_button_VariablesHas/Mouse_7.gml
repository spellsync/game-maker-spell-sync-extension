if (is_clicked) {
	input_dialog("Variables key", key, function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Variables key: " + _result;
			log(_msg);
			_msg = SpellSync_VariablesHas(_result) > 0 ? "Yes" : "No";
			_msg = "Variables " + string(_result) + " exist: " + _msg;
		    log(_msg);
		}
	});
};