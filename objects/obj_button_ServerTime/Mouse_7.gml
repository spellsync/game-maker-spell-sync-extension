if (is_clicked) {
	var _t = SpellSync_ServerTime();
	_msg = "Server Time: " + _t;
    log(_msg);
	var _tz = date_get_timezone();
	date_set_timezone(timezone_utc);
	var _d = convert_iso8601_to_datetime(_t);
	_msg = "DateTime: " + convert_datetime_to_iso8601(_d);
	log(_msg);
	date_set_timezone(_tz);
};