if (is_clicked) {
	input_dialog("Achievements Tag", key, function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Achievements id or tag: " + string(_result);
			log(_msg);
		    log("Achievements Unlock");
			SpellSync_AchievementsUnlock(_result);
		}
	});
};