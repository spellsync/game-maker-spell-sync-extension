if (is_clicked) {
	input_dialog("Share Param key", "gift", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Share Param key: " + string(_result);
			log(_msg);
			var _value = SpellSync_SocialsGetShareParam(_result);
			_msg = "Socials Get Share Param: " + string(_value);
		    log(_msg);
		}
	});
};