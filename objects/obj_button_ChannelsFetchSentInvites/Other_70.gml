/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnFetchSentInvitesCanLoadMore:
				var _canLoadMore = async_load[? "value"];
			break;
			case SpellSync_CallOnFetchSentInvites:
				var _json = async_load[? "value"];
				var items = json_parse(_json);
				/*
				for (var _i = 0; _i < array_length(items); _i++) {
					var invite = items[_i];
					invite.channel
				    invite.channel.id
				    invite.channel.tags
				    invite.channel.projectId
				    invite.channel.capacity
				    invite.channel.ownerId
				    invite.channel.name
				    invite.channel.description
				    invite.channel.private
				    invite.channel.visible
				    invite.channel.hasPassword
				    invite.channel.membersCount
				    invite.playerTo
				    invite.playerTo.id
				    invite.playerTo.name
				    invite.playerTo.avatar
				    invite.date
				}
				*/
			break;
			case SpellSync_CallOnFetchSentInvitesError:
				var _error = async_load[? "value"];
				//log(_error);
			break;
		}
   }
}

