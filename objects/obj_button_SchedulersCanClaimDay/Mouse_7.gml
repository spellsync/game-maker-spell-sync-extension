if (is_clicked) {
	input_dialog("Scheduler Id or Tag", "test_schedulers", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Schedulers Can Claim Day #2 " + _result + ": ";
			_msg += SpellSync_SchedulersCanClaimDay(_result, 2) > 0 ? "Yes" : "No";
			log(_msg);
		}
	});
};