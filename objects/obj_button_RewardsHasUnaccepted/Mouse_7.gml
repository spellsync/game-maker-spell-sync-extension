if (is_clicked) {
	input_dialog("Reward Id or Tag", "test_reward", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Rewards Has Unaccepted " + _result + ": ";
			_msg += SpellSync_RewardsHasUnaccepted(_result) > 0 ? "Yes" : "No";
			log(_msg);
		}
	});
};