if (is_clicked) {
	input_dialog("Change Language", "ru", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Set the language: " + string(_result);
			log(_msg);
			SpellSync_ChangeLanguage(_result);
		}
	});
};