if (is_clicked) {
	input_dialog("Images URL", "https://cdn.spellsync.com/static/images/661/988/661988f78e972aff45399703-512x512.webp", function() {
		if (string_length(input_result) > 0) {
			var _url = input_result;
			var _msg = "Images URL: " + string(_url);
			log(_msg);
	        var _w = 128;
	        var _h = 128;
			log("Images Width: " + string(_w));
			log("Images Height: " + string(_h));
		    log("Images Resize");
			var _new_url = SpellSync_ImagesResize(_url, _w, _h);
			_msg = "New Image: " + _new_url;
			log(_msg);
		}
	});
};