/// @description Async. Notifications about events in Channels
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnDeleteChannelEvent:
				var channelId = async_load[? "value"];
			break;
			case SpellSync_CallOnJoinEvent:
				var _json = async_load[? "value"];
				var member = json_parse(_json);
				/*
				member.channelId;
				member.id;
				member.state;
				member.mute;
				*/
			break;
			case SpellSync_CallOnCancelJoinEvent:
				var _json = async_load[? "value"];
				var joinRequest = json_parse(_json);
				/*
			    joinRequest.channelId;
			    joinRequest.playerId;
				*/
			break;
			case SpellSync_CallOnJoinRequestEvent:
				var _json = async_load[? "value"];
				var joinRequest = json_parse(_json);
				/*
			    joinRequest.channelId;
			    joinRequest.playerId;
			    joinRequest.player;
			    joinRequest.date;
				*/
			break;
			case SpellSync_CallOnLeaveEvent:
				var _json = async_load[? "value"];
				var memberLeave = json_parse(_json);
				/*
				memberLeave.channelId;
			    memberLeave.playerId;
			    memberLeave.reason;
				*/
			break;
			case SpellSync_CallOnMuteEvent:
				var _json = async_load[? "value"];
				var mute = json_parse(_json);
				/*
			    mute.channelId;
			    mute.playerId;
			    mute.unmuteAt;
				*/
			break;
			case SpellSync_CallOnUnmuteEvent:
				var _json = async_load[? "value"];
				var mute = json_parse(_json);
				/*
			    mute.channelId;
			    mute.playerId;
				*/
			break;
			case SpellSync_CallOnInviteEvent:
				var _json = async_load[? "value"];
				var invite = json_parse(_json);
				/*
			    invite.channelId;
			    invite.playerFromId;
			    invite.playerToId;
			    invite.date;
				*/
			break;
			case SpellSync_CallOnCancelInviteEvent:
				var _json = async_load[? "value"];
				var invite = json_parse(_json);
				/*
			    invite.channelId;
			    invite.playerFromId;
			    invite.playerToId;
				*/
			break;
			case SpellSync_CallOnRejectInviteEvent:
				var _json = async_load[? "value"];
				var invite = json_parse(_json);
				/*
			    invite.channelId;
			    invite.playerFromId;
			    invite.playerToId;
				*/
			break;
			case SpellSync_CallOnRejectJoinRequestEvent:
				var _json = async_load[? "value"];
				var joinRequest = json_parse(_json);
				/*
			    joinRequest.channelId;
			    joinRequest.playerId;
				*/
			break;
			case SpellSync_CallOnMessageEvent:
				var _json = async_load[? "value"];
				var _message = json_parse(_json);
				/*
			    _message.channelId;
			    _message.id;
			    _message.authorId;
			    _message.player;
			    _message.text;
			    _message.tags;
			    _message.createdAt;
				*/
			break;
			case SpellSync_CallOnEditMessageEvent:
				var _json = async_load[? "value"];
				var _message = json_parse(_json);
				/*
			    _message.channelId;
			    _message.id;
			    _message.authorId;
			    _message.text;
			    _message.tags;
			    _message.createdAt;
				*/
			break;
			case SpellSync_CallOnDeleteMessageEvent:
				var _json = async_load[? "value"];
				var _message = json_parse(_json);
				/*
			    _message.channelId;
			    _message.id;
			    _message.authorId;
			    _message.text;
			    _message.tags;
			    _message.createdAt;
				*/
			break;
		}
   }
}

