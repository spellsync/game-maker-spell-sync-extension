/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnFetchMembersCanLoadMore:
				var _canLoadMore = async_load[? "value"];
			break;
			case SpellSync_CallOnFetchMembers:
				var _json = async_load[? "value"];
				var items = json_parse(_json);
				/*
				for (var _i = 0; _i < array_length(items); _i++) {
					var member = items[_i];
					member.id
				    member.isOnline
				    member.state
				    member.state.name
				    member.state.avatar
				    member.state.score
				    member.mute
				    member.mute.isMuted
				    member.mute.unmuteAt
				}
				*/
			break;
			case SpellSync_CallOnFetchMembersError:
				var _error = async_load[? "value"];
				//log(_error);
			break;
		}
   }
}

