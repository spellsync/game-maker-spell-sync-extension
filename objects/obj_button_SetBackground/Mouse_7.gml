if (is_clicked) {
	log("Set Background #1");
	var _url = "https://s3.spellsync.com/games/files/4/f07c2ed5853d46658762db170834501f.jpeg";
	var _blur = 10; // Blur (px)
	var _fade = 5; // Fade (sec)
	SpellSync_SetBackground(_url, _blur, _fade);
	log("Wait to set a second background...");
	alarm[0] = room_speed * 5;
};