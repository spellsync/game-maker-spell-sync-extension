if (is_clicked) {
	input_dialog("Games Collections Tag", "ALL", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Games Collections Tag: " + string(_result);
			log(_msg);
		    log("Games Collections Open");
			SpellSync_GamesCollectionsOpen(_result);
		}
	});
};