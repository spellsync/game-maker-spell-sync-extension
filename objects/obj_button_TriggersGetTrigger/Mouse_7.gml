if (is_clicked) {
	input_dialog("Trigger Tag", "test_trigger", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _json = SpellSync_TriggersGetTrigger(_result);
			var _msg = "Triggers Get Trigger " + _result + ": ";
			if (string_length(_json) > 0) {
				_msg += _json;
				// var t = json_parse(_json);
				// t.trigger.id
			    // t.trigger.tag
			    // t.trigger.isAutoClaim
			    // t.trigger.description
			    // t.trigger.conditions
			    // t.trigger.bonuses
				//
				// t.isActivated
				// t.isClaimed
			}
			else {
				_msg += "trigger_not_found";
			}
			log(_msg);
		}
	});
};