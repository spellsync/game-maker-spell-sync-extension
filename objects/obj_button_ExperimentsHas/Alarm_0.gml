input_dialog("Experiments Cohort", "test_cohort", function() {
	if (string_length(input_result) > 0) {
		var _result = input_result;
		log("Experiments Cohort: " + _result);
		var _msg = "Is the player included in the cohort? ";
		_msg += SpellSync_ExperimentsHas(tag, _result) > 0 ? "Yes" : "No";
		log(_msg);
	}
});