if (is_clicked) {
	input_dialog("Player Add Score", 30, function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Player add Score: " + string(_result);
			log(_msg);
			SpellSync_PlayerAddScore(_result);
		}
	});
};