if (is_clicked) {
	input_dialog("Achievements Tag", key, function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Achievements id or tag: " + _result;
			log(_msg);
			_msg = SpellSync_AchievementsHas(_result) > 0 ? "Yes" : "No";
			_msg = "Achievements " + string(_result) + " exist: " + _msg;
		    log(_msg);
		}
	});
};