input_dialog("Leaderboard Scoped Variant", variant, function() {
	if (string_length(input_result) > 0) {
	    var _result = input_result;
		var _msg = "Leaderboard Scoped Variant: " + string(_result);
		log(_msg);
		log("Leaderboard Scoped Publish Record");
		SpellSync_LeaderboardScopedPublishRecord(key, _result, 1, "score", 10, "level", 5, "exp", 1);
	}
});