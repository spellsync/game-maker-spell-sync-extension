/// @description Checking the SDK ready
if (max_waiting_seconds > 0) {
	if (SpellSync_InitStatus() > 0) {
		// Initializing Player data
		// We recommend sending a message about the ready of the game after all the data is ready.
		SpellSync_GameStart();
		room_goto(room_demo);
	}
}
else {
	log("SDK initialization waiting error", true);
	room_goto(room_demo);
}
max_waiting_seconds -= 1;
alarm[0] = room_speed;