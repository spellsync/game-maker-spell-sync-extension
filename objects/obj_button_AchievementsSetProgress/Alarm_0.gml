input_dialog("Achievements Progress", "10", function() {
	if (string_length(input_result) > 0) {
		var _result = real(input_result);
		var _msg = "Achievements progress: " + string(_result);
		log(_msg);
		_msg = "Achievements Set Progress";
		log(_msg);
		SpellSync_AchievementsSetProgress(key, _result);
	}
});