text = "";
is_clicked = false;
is_disabled = true;
req_id = -1;
layer_name = "";

init = function() {
	if (string_length(layer_name) > 0) {
		if (layer_exists(layer_name)) {
			is_disabled = layer_get_visible(layer_name);
			if (is_disabled) {
				instance_activate_layer(layer_name);
			}
			else {
				instance_deactivate_layer(layer_name);
			}
			check_and_activate_buttons();
		}
	}
}
alarm[0] = 1;