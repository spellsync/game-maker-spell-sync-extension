if (is_clicked) {
	layer_set_visible(layer_name, true);
	with (obj_tabs_button_parent) {
		if (id != other.id) {
			layer_set_visible(layer_name, false);
		}
		init();
	}
	init();
}