if (is_clicked) {
	input_dialog("Player Set Name", "new name", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Player set Name: " + string(_result);
			log(_msg);
			SpellSync_PlayerSetName(_result);
		}
	});
};