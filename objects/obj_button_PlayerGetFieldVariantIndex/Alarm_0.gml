input_dialog("Player key variant value", "three", function() {
	if (string_length(input_result) > 0) {
	    var _result = is_numeric(input_result) ? real(input_result) : input_result;
		var _msg = "Player key variant value: " + string(_result);
		log(_msg);
		var _k = SpellSync_PlayerGetFieldVariantIndex(key, _result);
		_msg = "Player field variant Index: " + string(_k);
		log(_msg);
	}
});