if (is_clicked) {
	input_dialog("Scheduler Id or Tag", "test_schedulers", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Schedulers Is Today Reward Claimed " + _result + ": ";
			_msg += SpellSync_SchedulersIsTodayRewardClaimed(_result) > 0 ? "Yes" : "No";
			log(_msg);
		}
	});
};