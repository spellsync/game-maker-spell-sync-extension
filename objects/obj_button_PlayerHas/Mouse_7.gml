if (is_clicked) {
	input_dialog("Player key has", key, function() {
		if (string_length(input_result) > 0) {
	        key = input_result;
			var _msg = "Player has key: " + key;
			log(_msg);
			_msg = "Player has: " + (SpellSync_PlayerHas(key) > 0 ? "Exists" : "Not found");
			log(_msg);
		}
	});
};