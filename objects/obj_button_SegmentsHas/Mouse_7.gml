if (is_clicked) {
	input_dialog("Segments Tag", "test", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Segments Has " + _result + ": ";
			_msg += SpellSync_SegmentsHas(_result) > 0 ? "Yes" : "No";
			log(_msg);
		}
	});
};