/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
	    var _msg = json_encode(async_load);
		var _err = (async_load[? "event"] == SpellSync_CallRuntimeError);
		log_event(_msg, _err);
   }
}