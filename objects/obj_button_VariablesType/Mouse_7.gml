if (is_clicked) {
	input_dialog("Variables key", key, function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Variables key: " + _result;
			log(_msg);
			_msg = string(SpellSync_VariablesType(_result));
			_msg = "Variables " + string(_result) + " type: " + _msg;
		    log(_msg);
			
			// Checking the type of a variable
			switch (_result) {
				case SpellSync_VarTypeData:
					// string
				break;
				case SpellSync_VarTypeStats:
					// number
				break;
				case SpellSync_VarTypeDocHtml:
					// text in html format
				break;
				case SpellSync_VarTypeFlag:
					// boolean
				break;
				case SpellSync_VarTypeImage:
					// image link
				break;
				case SpellSync_VarTypeFile:
					// file link
				break;
				case SpellSync_VarTypeEmpty:
					// error getting a variable
				break;
			}
		}
	});
};