/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnFetchMoreChannelsCanLoadMore:
				var _canLoadMore = async_load[? "value"];
			break;
			case SpellSync_CallOnFetchMoreChannels:
				var _json = async_load[? "value"];
				var items = json_parse(_json);
				/*
				for (var _i = 0; _i < array_length(items); _i++) {
					var channel = items[_i];
				    channel.id
				    channel.tags
				    channel.templateId
				    channel.capacity
				    channel.ownerId
				    channel.name
				    channel.description
				    channel.private
				    channel.visible
				    channel.hasPassword
				    channel.membersCount
				}
				*/
			break;
			case SpellSync_CallOnFetchMoreChannelsError:
				var _error = async_load[? "value"];
				//log(_error);
			break;
		}
   }
}

