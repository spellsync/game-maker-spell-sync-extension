if (is_clicked) {
	var _j = SpellSync_PlayerToJSON();
	if (string_length(_j) > 0) {
		var _obj = json_decode(_j);
		var msg = "Player leaded to JSON: " + string(_j);
	    log(msg);
		//
		var _t = _obj[? "test_str"];
		msg = "Read 'test_str': " + string(_t);
	    log(msg);
		//
		_obj[? "test_str"] = _t + "_new";
		//
		var _json = json_encode(_obj);
		var msg = "Player save from JSON: " + string(_json);
	    log(msg);
		//
		SpellSync_PlayerFromJSON(_json);
	}
};