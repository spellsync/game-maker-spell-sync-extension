input_dialog("Player ID", 0, function() {
	if (string_length(input_result) > 0) {
		//
		var playerID = string_digits(input_result);
		SpellSync_ChannelsMuteSeconds(channelID, playerID, 30);
	};
});