if (is_clicked) {
	input_dialog("Leaderboard Scoped Tag", key, function() {
		if (string_length(input_result) > 0) {
	        key = input_result;
			var _msg = "Leaderboard Scoped Tag: " + string(key);
			log(_msg);
			alarm[0] = 1;
		}
	});
};