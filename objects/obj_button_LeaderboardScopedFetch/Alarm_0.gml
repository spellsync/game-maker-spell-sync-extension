input_dialog("Leaderboard Scoped Variant", variant, function() {
	if (string_length(input_result) > 0) {
	    var _result = input_result;
		var _msg = "Leaderboard Scoped Variant: " + string(_result);
		log(_msg);
		log("Leaderboard Scoped Fetch");
		SpellSync_LeaderboardScopedFetch(key, _result, SpellSync_OrderDESC, 10, "rank", SpellSync_LeaderboardWithMeFirst);
	}
});