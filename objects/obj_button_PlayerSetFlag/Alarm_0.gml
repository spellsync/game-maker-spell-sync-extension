input_dialog("Player flag value", 1, function() {
	if (string_length(input_result) > 0) {
		var _result = real(input_result);
		_result = _result > 0 ? 1 : 0
		var _msg = "Player Set value: " + string(_result);
		log(_msg);
		SpellSync_PlayerSetFlag(key, _result);
	}
});