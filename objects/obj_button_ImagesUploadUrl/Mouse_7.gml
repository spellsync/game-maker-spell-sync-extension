if (is_clicked) {
	input_dialog("Images URL", "data:image/png;base64,R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", function() {
		if (string_length(input_result) > 0) {
			var _url = input_result;
			var _msg = "Images URL: " + string(_url);
			log(_msg);
	        var _tags = "replay,level7";
			var _msg = "Images Tags: " + string(_tags);
			log(_msg);
		    log("Images Upload URL");
			SpellSync_ImagesUploadUrl(_url, "test_img.png", _tags);
		}
	});
};