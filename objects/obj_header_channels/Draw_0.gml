/// @description Draw a description of the problem
event_inherited();
//
if (string_length(onlineservice_text) > 0) {
	var _text_height = string_height(text);
	draw_set_valign(fa_middle);
	draw_set_font(fnt_mini);
	draw_set_colour(c_red);
	draw_text(x + sprite_get_width(sprite_index) + 7, y + round(_text_height / 2), onlineservice_text);
}