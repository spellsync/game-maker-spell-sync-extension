/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallChangeOrientation:
				//
				var _isPortrait = async_load[? "value"];
				SpellSync_LoggerWarn(_isPortrait);
			break;
		}
   }
}

