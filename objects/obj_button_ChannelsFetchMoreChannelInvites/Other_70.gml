/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnFetchMoreChannelInvitesCanLoadMore:
				var _canLoadMore = async_load[? "value"];
			break;
			case SpellSync_CallOnFetchMoreChannelInvites:
				var _json = async_load[? "value"];
				var items = json_parse(_json);
				/*
				for (var _i = 0; _i < array_length(items); _i++) {
					var invite = items[_i];
					invite.playerFrom
					invite.playerFrom.id
					invite.playerFrom.name
					invite.playerFrom.avatar
					invite.playerTo
					invite.playerTo.id
					invite.playerTo.name
					invite.playerTo.avatar
					invite.date
				}
				*/
			break;
			case SpellSync_CallOnFetchMoreChannelInvitesError:
				var _error = async_load[? "value"];
				//log(_error);
			break;
		}
   }
}

