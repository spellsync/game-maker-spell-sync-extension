if (is_clicked) {
	input_dialog("Share text", "Join me in the game \"My awesome game 2\"", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Share text: " + string(_result);
			log(_msg);
		    log("Socials Post");
			SpellSync_SocialsPost(_result);
		}
	});
};