if (is_clicked) {
	input_dialog("Player key name", key, function() {
		if (string_length(input_result) > 0) {
	        key = input_result;
			var _msg = "Player flag key: " + string(key);
			log(_msg);
			SpellSync_PlayerToggle(key);
		}
	});
};