if (is_clicked) {
	input_dialog("Trigger Id or Tag", "test_trigger", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Triggers Is Activated " + _result + ": ";
			_msg += SpellSync_TriggersIsActivated(_result) > 0 ? "Yes" : "No";
			log(_msg);
		}
	});
};