if (is_clicked) {
	input_dialog("Payments Purchase", "test", function() {
		if (string_length(input_result) > 0) {
	        var _result = input_result;
			var _msg = "Payments Purchase: " + string(_result);
			log(_msg);
			SpellSync_PaymentsPurchase(_result);
		}
	});
};