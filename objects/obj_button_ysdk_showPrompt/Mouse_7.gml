if (is_clicked) {
	if (SpellSync_PlatformType() != SpellSync_PlatformTypeYANDEX) {
		log("The method is only for the Yandex Games platform.", true);
	}
	else {
		log("Call native yandex shortcut.showPrompt.");
		SpellSync_PlatformGetNativeSDK("shortcut.showPrompt", req_event);	
	}
};