/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallAdsStart:
				//
			break;
			case SpellSync_CallOnPause:
				//
			break;
			case SpellSync_CallAdsFullscreenStart:
				//
			break;
			case SpellSync_CallAdsClose:
				if (async_load[? "value"] == "1") {
					// The ad was successfully displayed
				}
			break;
			case SpellSync_CallOnResume:
				//
			break;
			case SpellSync_CallAdsFullscreenClose:
				if (async_load[? "value"] == "1") {
					// The ad was successfully displayed
				}
			break;
		}
   }
}

