if (is_clicked) {
	input_dialog("Scheduler Id or Tag", "test_schedulers", function() {
		if (string_length(input_result) > 0) {
		    var _result = input_result;
			var _msg = "Schedulers Claim Day #3: " + _result;
			log(_msg);
			SpellSync_SchedulersClaimDayAdditional(_result, 3, "test_additional");
		}
	});
};