/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnFetchMessagesCanLoadMore:
				var _canLoadMore = async_load[? "value"];
			break;
			case SpellSync_CallOnFetchMessages:
				var _json = async_load[? "value"];
				var items = json_parse(_json);
				/*
				for (var _i = 0; _i < array_length(items); _i++) {
					var message = items[_i];
					message.id
					message.channelId
					message.authorId
					message.text
					message.tags
					message.player
					message.player.id
					message.player.name
					message.player.avatar
					message.createdAt
				}
				*/
			break;
			case SpellSync_CallOnFetchMessagesError:
				var _error = async_load[? "value"];
				//log(_error);
			break;
		}
   }
}

