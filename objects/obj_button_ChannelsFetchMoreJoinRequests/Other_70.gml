/// @description Output to the console of all responses from SpellSync
if (not isMap(async_load)) {
    log("async_load got lost in cyberspace");
} else {
    if (async_load[? "type"] == SpellSync_AsyncEvent) {
		// Logging
		switch (async_load[? "event"])
		{
			case SpellSync_CallOnFetchMoreJoinRequestsCanLoadMore:
				var _canLoadMore = async_load[? "value"];
			break;
			case SpellSync_CallOnFetchMoreJoinRequests:
				var _json = async_load[? "value"];
				var items = json_parse(_json);
				/*
				for (var _i = 0; _i < array_length(items); _i++) {
					var joinRequest = items[_i];
					joinRequest.player
				    joinRequest.player.id
				    joinRequest.player.name
				    joinRequest.player.avatar
				    invite.date
				}
				*/
			break;
			case SpellSync_CallOnFetchMoreJoinRequestsError:
				var _error = async_load[? "value"];
				//log(_error);
			break;
		}
   }
}

