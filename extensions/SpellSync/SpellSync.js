var SpellSyncGMS = {
	_mapTypeDesc: "SpellSync",
	_allowConsoleDebug: false,
	ss: null,
	sdkReady: false,

	/**
	 * Translating the object into a string
	 * @param e
	 * @returns {string}
	 */
	toString: function (e) {
		try {
			switch (typeof e) {
				case "number":
					return String(e);
				case "boolean":
					return String(e ? 1 : 0);
				case "string":
					return e;
			}
			return JSON.stringify(e);
		} catch (error) {
			return JSON.stringify({error: error});
		}
	},

	/**
	 * Output data to the browser console (only if debugging is enabled)
	 * @param {String} message
	 * @param {any} data
	 * @returns {Number} 1
	 */
	browserConsoleLog: function (message, data = null) {
		let self = SpellSyncGMS;
		if (self._allowConsoleDebug) {
			if (data) {
				message += " | " + self.toString(data);
			}
			console.log(message);
		}
		return 1;
	},

	/**
	 * Page reload function
	 * @returns {Number} 1
	 */
	pageReload: function() {
		SpellSyncGMS.browserConsoleLog( "Reloading the browser page");
		setTimeout(() => {
			window.location.reload();
		}, 0);
		return 1;
	},

	/**
	 * Get the SDK initialization status
	 * @returns {Number} 1 0
	 */
	getInitStatus: function() {
		let self = SpellSyncGMS;
		if (self.ss != null) {
			if ((self.ss) && (self.ss.sdkReady)) {
				self.browserConsoleLog("SDK is Initiated");
				return 1;
			}
		}
		self.browserConsoleLog("SDK not Initiated");
		return 0;
	},

	/**
	 * Allow log output to the browser console
	 * @param debugStatus {Number} Enable (1) / Disable (0)
	 * @returns {Number} Enabled (1) / Disabled (0)
	 */
	setDebugMode: function(debugStatus) {
		let self = SpellSyncGMS;
		if (debugStatus > 0) {
			if (!self._allowConsoleDebug) {
				console.log("Browser debug mode enabled");
				self._allowConsoleDebug = true;
			}
		}
		else {
			if (self._allowConsoleDebug) {
				console.log("Browser debug mode disabled");
				self._allowConsoleDebug = false;
			}
		}
		return (self._allowConsoleDebug ? 1 : 0);
	},

	/**
	 * Get is allow log output to the browser console
	 * @returns {Number} Enabled (1) / Disabled (0)
	 */
	getDebugMode: function() {
		return (SpellSyncGMS._allowConsoleDebug ? 1 : 0);
	},

	/**
	 * Get Browser priority language
	 * @returns {String}
	 */
	getBrowserLang: function() {
		let brLang = navigator.language || navigator.userLanguage;
		SpellSyncGMS.browserConsoleLog( "Browser priority language: " + brLang);
		return brLang;
	},
}

/**
 * Waiting for the SDK to be ready from GameMaker.
 * @returns {number}
 */
function SS_SDK_Init() {
	try {
		if (SpellSyncGMS.getInitStatus) {
			SpellSyncGMS.sdkReady = true;
		}
		else {
			setTimeout(SS_SDK_Init, 200);
		}
	} catch (err) {
		console.error(err);
	}
	return 1;
}

/**
 * Copy text to the clipboard in modern browsers
 * https://github.com/sindresorhus/copy-text-to-clipboard
 * @param input
 * @param target
 * @returns {number}
 */
function copyTextToClipboard(input, {target = document.body} = {}) {
	const element = document.createElement('textarea');
	const previouslyFocusedElement = document.activeElement;

	element.value = input;

	// Prevent keyboard from showing on mobile
	element.setAttribute('readonly', '');

	element.style.contain = 'strict';
	element.style.position = 'absolute';
	element.style.left = '-9999px';
	element.style.fontSize = '12pt'; // Prevent zooming on iOS

	const selection = document.getSelection();
	const originalRange = selection.rangeCount > 0 && selection.getRangeAt(0);

	target.append(element);
	element.select();

	// Explicit selection workaround for iOS
	element.selectionStart = 0;
	element.selectionEnd = input.length;

	let isSuccess = false;
	try {
		isSuccess = document.execCommand('copy');
	} catch {}

	element.remove();

	if (originalRange) {
		selection.removeAllRanges();
		selection.addRange(originalRange);
	}

	// Get the focus back on the previously focused element, if any
	if (previouslyFocusedElement) {
		previouslyFocusedElement.focus();
	}

	return (isSuccess ? 1 : 0);
}

(function (d) {
	function waitFor(check) {
		return new Promise((resolve) => {
			let intervalId = 0;
	
			function checkReady() {
				if (check(window)) {
					clearInterval(intervalId);
					resolve();
				}
			}
	
			if (check(window)) {
				resolve();
				return;
			}
	
			intervalId = setInterval(checkReady, 100);
		});
	}

	async function setupSS() {
		await waitFor((w) => !!w.SpellSyncClass);
		SpellSyncGMS.ss = new SpellSyncClass(window.ss);
		window.ss = null;
		// Show Ads
		try {
			if (SpellSync_Send_game_start) {
				SpellSyncGMS.ss.GameStart();
			}
			if (SpellSync_Show_preloader_ad) {
				SpellSyncGMS.ss.AdsShowPreloader();
			}
			if (SpellSync_Show_sticky_ad) {
				SpellSyncGMS.ss.AdsShowSticky();
			}
		} catch (err) {
			console.error(err);
		}
	}

	if (window.ss == null) {
		console.log('SpellSync SDK start load script');
		if (SpellSync_Project_ID === null) {
			console.error('SpellSync Project ID not found!');
		}
		else if (SpellSync_Public_token === null) {
			console.error('SpellSync Public token not found!');
		}
		else {
			window.SpellSyncConfig = {
				projectId: Number(SpellSync_Project_ID),
				publicToken: SpellSync_Public_token,
				onReady: (ss) => {
					try {
						if (ss) window.ss = ss;
						if (window.ss) {
							setupSS();
						}
					} catch (err) {
						console.error(err);
					}
				}
			};

			let t = d.getElementsByTagName('script')[0];
			let s = d.createElement('script');
			s.src = '//s3.spellsync.com/sdk/spellsync.js';
			s.async = true;
			t.parentNode.insertBefore(s, t);
		}
	} else {
		setupSS();
	}
})(document);

// /**
//  * Waiting for the Class to be ready
//  */
// initWait = function() {
// 	setTimeout(() => {
// 		let is_defined = false;
// 		try {
// 			if (typeof SpellSyncClass !== 'undefined') {
// 				is_defined = true;
// 				window.SpellSyncConfig.onReady();
// 			}
// 		}
// 		catch (err) {
// 			console.error(err);
// 		}
// 		if (!is_defined) {
// 			initWait();
// 		}
// 	}, 100);
// }
// initWait();